let express = require("express")
let myApp = express()
let mongodb = require("mongodb")
myApp.use(express.static("public"))
myApp.use(express.urlencoded({ extended: false }))

myApp.use(express.json())
// let Ssanitize-html = require("sanitize-html")

let db 
let port = process.env.PORT
if (port == null || port == ""){
  port = 3000

}
let strings = "mongodb+srv://todoapp:todoapp@cluster0-bbn1f.mongodb.net/ToDoApp?retryWrites=true&w=majority"
myApp.use(passwordProtected)
mongodb.connect(strings , {useNewUrlParser: true}, function(err , client){

    db = client.db()
    myApp.listen(3000)
})

function passwordProtected(req , res , next){

  res.set('WWW-Authenticate','Basic realm =""')
        if (req.headers.authorization=="Basic YWRtaW46YWRtaW4=")
    {
      next()
    }
    else{
      res.status(401).send("user don't have access to app")
    }

}




myApp.get('/',  function (req, res) {
    
  
  db.collection("items").find().toArray(function(err , items){
     
    res.send(`<!DOCTYPE html>
    <html>
    <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Simple To-Do App</title>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    </head>
    <body>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="/browser.js"></script>
      <div class="container">
        <h1 class="display-4 text-center py-1">To-Do App</h1>
        
        <div class="jumbotron p-3 shadow-sm">
          <form action  = "/create-item" method = "POST">
            <div class="d-flex align-items-center">
              <input name = "item" autofocus autocomplete="off" class="form-control mr-3" type="text" style="flex: 1;">
              <button class="btn btn-primary">Add New Item</button>
            </div>
          </form>
        </div>
        <button id = "btn3">CLICK ME</button>
        
        <ul class="list-group pb-5">
          ${items.map(function(data){
            return `<li class="list-group-item list-group-item-action d-flex align-items-center justify-content-between">
            <span class="item-text">${data.text}</span>
            <div>
                <button data-id ="${data._id}" class="edit-me btn btn-secondary btn-sm mr-1">Edit</button>
              <button data-id="${data._id}" class ="delete-me btn btn-danger btn-sm">Delete</button>
            </div>
          </li>`
          }).join("")}
        </ul>
        
      </div>
       
    </body>
    </html>`)

  })
    
})


myApp.post("/create-item", function(req , res){
     
    // let securityauthenticate = sanitize-html
    db.collection("items").insertOne({text : req.body.item}, function(){

    res.redirect("/")
   })})
 

   myApp.post("/update-item", function(req , res){
    db.collection("items").findOneAndUpdate({_id: new mongodb.ObjectID(req.body.id)}, {$set: {text: req.body.text}},function(){
      res.send("Success")
    })
  })

  myApp.post("/delete-item",function(req , res){
    db.collection("items").removeOne({_id: new mongodb.ObjectID(req.body.id)}, function(){
      res.send("SUCESS")
    })
  })
  